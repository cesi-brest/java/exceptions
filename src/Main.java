import exceptions.ExceptionMaison;
import exceptions.ExceptionRepas;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        for (int heure = 0; heure < 24; heure++) {
            try {
                auBoulot(heure);
                System.out.printf("au boulot : %d%n", heure);
            } catch (Exception e) {
                System.out.printf("%s%n", e.getMessage());
                //Permet d'afficher le message qu'on a param ExceptionRepas
            }
        }


    }

    private static void auBoulot(int heure) throws ExceptionMaison, ExceptionRepas {
        if (heure < 8) {
            throw new ExceptionMaison(heure);
        }
        if ((heure >= 12) && (heure < 14)) {
            throw new ExceptionRepas(heure);
        }
        if (heure > 17) {
            throw new ExceptionMaison(heure);
        }
    }
}

