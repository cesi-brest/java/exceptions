package exceptions;

public class ExceptionMaison extends Exception {
    public ExceptionMaison(int heure) {
        super(String.format("à la maison : %d", heure));
    }
}
