package exceptions;

public class ExceptionRepas extends Exception {
    public ExceptionRepas(int heure) {
        super(String.format("entrain de manger : %d", heure));
    }
}
